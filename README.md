# Clover config for Lenovo Yoga 720-15 IKB

## Hardware
 
| Part | Hardware |
|------|----------|
| CPU | Intel Core i5 7300 HQ |
| Chipset | Intel HM 175 |
| RAM | 8 GB DDR4 2133 MHz |
| GPU #1 | NVidia GTX 1050 2GB |
| GPU #2 | Intel HD Graphics 630 |
| WLAN| DW1820A version CN-0VW3T3 |
| Display | BOEhydis NV156FHM-N61  [15.6" LCD] |
| Audio #1 | IKB HDMI |
| Audio #2 | Realtek ALC236 |
| SSD | Intel SSD 660p (NVMe) |
| Keyboard | Standard PS/2 Keyboard |
| Touchpad | HID-compilant mouse |
| TB3 Controller | Intel JHL6240 |
| USB3 Controller | Intel Sunrise Point PCH - USB 3.0 xHCI Controller |

## Problems
* with config based on Vesavlad’s I can boot to installer, create APFS partition. In stage 2, after 1st reboot I’m getting this [message](https://imgur.com/a/4yI1lMV). 	
* disabling Intel TPT does not allow me to get into installer. Nothing,  just flashing black screen and back to clover and/or Windows 10  (according to boot settings). [This](https://imgur.com/gallery/SI7fGAs) is the verbose output. Last message is:
`VoodooPS2Trackpad sending final init sequence failed: 0`
* creating installer from MacOS 10.9 Mavericks works best for me. After  making installer form Mojave, I was stuck on Prohibited sign. [Kernel Panic](https://imgur.com/gallery/WqDI9Oz) verbose output.

## My BIOS settings:
* Configuration 		
   * Wireless LAN \[Disabled\] 			
   * SATA Controller Mode \[AHCI\] 			
   * Graphic Device \[Intel UMA\] 			
   * Power Beep \[Disabled\] 			
   * Intel Virtualization Technology \[Disabled\] 			
   * Bios Back Flash \[Disabled\] 			
   * Hotkey Mode \[Disabled\] 			
   * Always On USB \[Enabled\] 			
   * Thunderbolt (TM) Device Boot Support \[Disabled\] 			
   * Thermal Control \[Performance\] 			
* Security 		
   * Intel Platform Trust Technology \[Enabled\] 			
   * Intel SGX \[Disabled\] 			
   * Secure Boot \[Disabled\] 			
* Boot 		
   * Boot Mode \[UEFI\] 			
   * Fast Boot \[Disabled\] 			
   * USB Boot \[Enabled\] 			
   * PXE Boot to LAN \[Disabled\] 			
   * IPV4 PXE First \[Disabled\]
